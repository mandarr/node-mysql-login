const jwt = require("jsonwebtoken");
require("dotenv").config();

const JWT_SECRET_KEY = process.env.SECRET_KEY;

module.exports = (req, res, next) => {
  try {
    const token = req.header("x-access-token");
    if (!token) {
      return res.status(403).json({ message: "Access Denied." });
    }

    const verifyToken = jwt.verify(token, JWT_SECRET_KEY);
    next();
  } catch (error) {
    res.status(400).send("Invalid Token");
  }
};
