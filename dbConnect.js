const mysql = require("mysql2");

// Read dotenv files
require("dotenv").config();

// Create mysql connection
const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

module.exports = db;

/* Reason behind using mysql2 over mysql
1. MySQL 8 supports pluggable authentication methods. By default, one of them name `caching_sha2_password` is used rather than old `mysql_native_password`.
2. A crypto algo with serveral handshakes is more secure than plain password passing.
3. Node `mysql` package doesn't support this new default authentication method of MySQL 8, yet.
4. Node `mysql2` supports new authentication methods and is a forked work off of the popular `mysql`.
*/
