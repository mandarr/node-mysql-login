FROM node:14.16.0-alpine3.13

WORKDIR /usr/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8000

CMD ["yarn","server"]