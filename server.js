const express = require("express");
const app = express();
const db = require("./dbConnect");
const PORT = process.env.PORT || 8000;
const userRoute = require("./routes/users");
const cookieParser = require("cookie-parser");

// Parse json data
app.use(express.json());

app.use(cookieParser());

// user route
app.use("/users", userRoute);

app.get("/", (req, res) => {
  res.json({ message: "Node-MySQL-Login API ⚡" });
});

// Connect to db
db.connect((err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Connected to MySQL Database successfully..");
  }
});

// Listen to port 8000
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
