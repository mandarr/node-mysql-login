// Read dotenv files
require("dotenv").config();

const jwt = require("jsonwebtoken");

// bcrypt for hashing plain text password
const bcrypt = require("bcryptjs");

const db = require("../dbConnect");

const JWT_SECRET_KEY = process.env.SECRET_KEY;

// Register user
const userRegister = async (req, res) => {
  const { username, email, password } = req.body;

  if (email && username && password) {
    // validation
    db.query(
      "SELECT username, email FROM user_details WHERE username = ? AND email = ?",
      [username, email],
      (error, result) => {
        if (error) {
          console.log(error);
        }
        if (result.length > 0) {
          return res.json({ message: "Email or username already exists" });
        }
      }
    );

    // Insert username & email in user_details table
    db.query(
      "INSERT INTO user_details SET ?",
      {
        username: username,
        email: email,
      },
      (error, result) => {
        if (error) {
          console.log(error);
        } else {
          return res
            .status(200)
            .json({ message: "User successfully registered" });
        }
      }
    );

    const hashedPassword = await bcrypt.hash(password, 10);
    // console.log(hashedPassword);

    // Insert username & hashedPassword in user_auth table
    db.query(
      "INSERT INTO user_auth SET ?",
      {
        username: username,
        password: hashedPassword,
      },
      (error, result) => {
        if (error) {
          console.log(error);
        }
      }
    );
  } else {
    res.status(400).json({ message: "Please fill out all required fields!" });
  }
};

// Get all users
const getAllUsers = async (req, res) => {
  db.query("SELECT * FROM user_details", (error, result) => {
    if (error) {
      console.log(error);
    } else {
      return res.status(200).json({ users: result });
    }
  });
};

// Get user details
const getUser = async (req, res) => {
  const { id } = req.params;
  db.query("SELECT * FROM user_details WHERE id = ?", [id], (error, result) => {
    if (error) {
      console.log(error);
    } else {
      return res.status(200).json({ user: result });
    }
  });
};

// User login
const userLogin = async (req, res) => {
  const { username, password } = req.body;

  const data = db.query(
    "SELECT id,password  FROM user_auth WHERE username = ?",
    [username],
    (error, result) => {
      if (error) {
        console.log(error);
      } else {
        if (result.length <= 0) {
          return res
            .status(400)
            .json({ message: "Please enter correct username/password" });
        } else {
          generateToken(...result);
        }
      }
    }
  );

  const generateToken = async (result) => {
    const data = { id: result.id, password: result.password };
    if (await bcrypt.compare(password, result.password)) {
      // Access Token
      const token = jwt.sign(data, JWT_SECRET_KEY, { expiresIn: "1h" });

      // Refresh Token
      const refreshToken = jwt.sign(data, JWT_SECRET_KEY, {
        expiresIn: "7d",
      });
      db.query("UPDATE user_auth SET refresh_token = ? WHERE id = ?", [
        refreshToken,
        result.id,
      ]);
      return res
        .status(200)
        .cookie("refresh_token", refreshToken, {
          httpOnly: true,
          secure: process.env.NODE_ENV === "production",
        })
        .json({ message: "Login Successful!", access_token: token });
    } else {
      res
        .status(400)
        .json({ message: "Please enter correct username/password" });
    }
  };
};

// Generate new Access-Token from Refresh-Token
const getAccessToken = async (req, res) => {
  const token = req.cookies.refresh_token;

  if (!token) {
    return res.status(403).json({ message: "Forbidden" });
  }

  jwt.verify(token, JWT_SECRET_KEY, async (error, result) => {
    if (error) {
      return res.status(403);
    }
    db.query(
      "SELECT id,password FROM user_auth WHERE refresh_token = ?",
      [token],
      (error, result) => {
        if (error) {
          console.log(error);
        } else {
          generateToken(...result);
        }
      }
    );
    const generateToken = async (result) => {
      const data = { id: result.id, password: result.password };

      // Access Token
      const token = jwt.sign(data, JWT_SECRET_KEY, { expiresIn: "1h" });

      // Refresh Token
      const refreshToken = jwt.sign(data, JWT_SECRET_KEY, {
        expiresIn: "7d",
      });
      db.query("UPDATE user_auth SET refresh_token = ? WHERE id = ?", [
        refreshToken,
        result.id,
      ]);
      return res
        .status(200)
        .cookie("refresh_token", refreshToken, {
          httpOnly: true,
          secure: process.env.NODE_ENV === "production",
        })
        .json({ message: "New Access-Token Generated", access_token: token });
    };
  });
};

module.exports = {
  userRegister,
  getAllUsers,
  getUser,
  userLogin,
  getAccessToken,
};
