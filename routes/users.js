const express = require("express");

const router = express.Router();

const auth = require("../middlewares/auth");

// userControllers
const userControllers = require("../controllers/userController");

// Register new user
router.post("/register", userControllers.userRegister);

// Login user
router.post("/login", userControllers.userLogin);

// Generate New Access Token
router.post("/token", userControllers.getAccessToken);

// API authentication middleware
router.use(auth);

// Get all users
router.get("/", auth, userControllers.getAllUsers);

// Get user
router.get("/:id", auth, userControllers.getUser);

module.exports = router;
